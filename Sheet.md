# LMU450 Cheat Sheet

### Långsiktiga lönsamhetsbedömningar

- G - Grundinvestering
- I - Betalning
- U - Betalning
- n - Livslängd eller planeringshorisont
- R - Restvärde
- r - Kalkylränta

## Resultatanalys

### Begrepp

- Alternativkostnad
- Direkta kostnader
- Fasta kostnader
- Inbetalning
- Indirekta kostnader
- Inkomst
- Intäkt
- Kostnad
- Rörliga kostnader
- Utbetalning
- Utgift

### Trivia

### Definitioner

- Alternativkostnad: Kostnad i intäckter jämfört med det bästa alternativet
- Bokföringsmässig kostnad: Enligt regler
- Död zon: Derivatan av resultatet med avseende på volym är negativt
- Effektivitet: måluppfyllelse / resusrsanvändning
- Ekonomi: Att hushålla med tillgängliga (begränsade) resurser
- Inbetalning: Kassaflöde in till företaget (vid betalning)
- Inkomst: Värdet av en såld resurs, uppstår vid försäljningstillfället registreras i bokföring när faktura skickas
- Intäkter: Värdet av levererade prestationer inom tidsinterval
- Kalkylmässig kostnad: Nukostnad
- Känslighetsanalys: Förändrade förutsättningars effekt på kalkyl
- Kostnader: Värdet av förbrukade resurser inom tidsinterval
- Likviditet: Betalningsströmmar, kassaflöde
- Periodkalkylering: Beräkning av självkostnad inom tidsinterval (Både förkalkyl och efterkalkyl)
- Produktivitet: Prestationer / resursförbrukning
- Resultatanalys: Kalkylering baserad på rörliga och fasta kostnader
- Resultat: Totala intäkter - Totala kostnader
- Självkostnad: Gräns för genomförbar långsiktig kostnaden per styck för produkt
- Totala kostnader: Fasta kostnader + Rörliga kostnader \* volym
- Utbetalning: Kassaflöde ut från företaget (vid betalning)
- Utgift: Värdet av en köpt resurs uppståanskaffningstillfället anskaffningstillfället registreras i bokföring när faktura erhålls

## Produktkalkylering I

### Definitioner

- Självkostnad: Långsiktigt gränsvärde för lönsamhet av vara/tjänst (kostnad/styck)
- Periodkalkylering: Beräkning av självkostnad under period
- Direkt kostnad: Enskildt kalkylobjekt rörligt eller fast
- Indirekt kostnad: Gemensam kostnad för kalkylobjekt i ett och samma kostnadsställe, rörligt eller fast
- Påläggskalkylering: Godtyckligt beräknad prisökning som kompenserar för omkostnad :)
- Fördelningsnyckel: Direkt kostnad i form av pålägg för att täcka olika omkostnader

### Periodkalkyler

- Divisionskalkyl: (TK inom tidsinterval) / (Verklig volym inom tidsinterval)
- Normalkalkyl
  - Utnyttjandegrad: Verklig Volym / Normal Volym
  - Självkostnad per enhet: (Fast kostnad / Normal volym) + (Rörlig kostnad / Verklig volym)
- Minimikalkyl:

  - Rörlig kostnad per enhet: (Verklig volym)

- Självkostnad för kalkylobjekt: Direkt kostnad + Approximerad andel av indirekt kostnad

### Påläggskalkylering

- Påläggssats: Total Omkostnad / Total direkt kostnad
  - Förkalkyl:
    - Normal volym
    - Budgeterad volym
    - Praktisk volym
  - Efterkalkyl
    - Verklig volym

## Produktkalkylering II

### Definitioner

- Påläggskalkylering: Systematisk fördelning av kostnader på kostnadsbärare
- Bidragskalkylering: Kalkylering baserad på särkostnader och samkostnader
- Stegkalkyl: Ett slags mellanting mellan ”ren” självkostnadskalkyl och ”ren” bidragskalkyl
- Trång sektion: Flaskhals eller begränsande faktor
- Restkalkyl: Kalkyl för kostnader på huvudprodukt som minskas av biproduktens TB

### Relaterade Begrepp

- ”Sunk cost”
- Täckningsbidrag (TB) = särintäkt – särkostnad
- Trång sektion
- Alternativkostnad

## Investeringskalkylering

### Definitioner

- Investering: Kapitalsatsning med långsiktiga betalningskonsekvenser
- Diskontering: Back to the future pengar fast tvärt om
- Kapitalisering: Back to the future pengar

### Begrepp

- Kalkylränta
- Livslängd

## Räkenskapsanalys

- Rt: Räntabilitet på totalt kapital
- Rt = (Rörelseresultat (EBIT) + finansiella intäkter) / Genomsnittligt Totalt kapital
- Rsyss: Räntabilitet på sysselsatt kapital
- Rsyss = (Rörelseresultat (EBIT) + finansiella intäkter) / (Genomsnittligt) avkastningskrävande kapital
- Re: Räntabilitet på eget kapital
- Re: (Resultat efter finansnetto) / (Genomsnittligt) Justerat eget kapital\*
- Rs: Genomsnittlig låneränta

### Likviditet

- Kassalikviditet: (omsättningstillgångar – varulager) / Kortfristiga skulder
- Balanslikviditet: Omsättningstillgångar / Kortfristiga skulder
- Soliditet I: Justerat EK\* / Totalt kapital
- Solidetet II: Riskbärande kapital (=EK+Obesk reserv) / Totalt kapital

## Förkortningar

- ABC - Activity based costing
- AO - Administrationsomkostnad
- AO+FO, AFFO - Affärsomkostnad
- ÅRL - Årsredovisningslagen
- BFL - Bokföringslagen
- DF - Direkta försäljningskostnader
- dL - Direkt lön
- DL - Direkt Lönekostnad
- dM - Direkt material
- DM - Direkt Matrialkostnad
- EBIT - Rörelseresultat
- EK - Eget kapital
- FK - Fast Kostnad
- FO - Försäljningsomkostnad
- LAS - Lagen om anställningsskydd
- MBL - Medbestämmandelagen
- MO - Materialomkostnad
- PIA - Produkter i arbete
- RK - Rörlig Kostnad
- SDT - Speciella Direkta tillverkningskostnader
- TB - Täckningsbidrag
- TG - Täckningsgrad
- TI-TK - Resultat
- TI - Total Intäckt
- TK - Total Kostnad
- TO - Tillverkningsomkostnad
- TTB - Totalt täckningsbidrag
- TVK - Tillverkningskostnad?

## Litteratur

Ekonomiska grundbegrepp och resultatplanering (kap 1-3)
Produktkalkylering, del 1 & 2 (kap 3-5)
Investeringskalkylering (kap 7-8)
Årsredovisning och finansiell analys, del 1 & 2 (kap 9-10)
